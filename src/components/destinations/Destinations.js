import React from 'react';
import { Row, Col, Button } from 'antd';
import {
  ArrowRightOutlined
} from '@ant-design/icons';

import './Destinations.css';

import DestinationsImage1 from '../../assets/images/nicolas-i-tVTvDFkT0Vw-unsplash 1.png';
import DestinationsImage2 from '../../assets/images/nurhadi-cahyono-3-gjm36-dYg-unsplash 1.png';
import DestinationsImage3 from '../../assets/images/mantas-hesthaven-_g1WdcKcV3w-unsplash 2.png';
import DestinationsImage4 from '../../assets/images/andy-holmes-0LJCEORiYg8-unsplash 2.png';
import DestinationsImage5 from '../../assets/images/elizeu-dias-SEq9dyZSe6c-unsplash 1.png';

const Destinations = () => {
	return (
		<>
			<div className="container">
				<h1>Top <span>Destinations</span> For Your <span>Holiday</span></h1>

				<Row gutter={[20, 20]}>
					<Col md={10} className="lg-destination-item">
						<div className="lg-destination-item-img">
							<img src={DestinationsImage1} />
						</div>
						<div className="lg-destination-item-desc">
							<span className="cntr-text">Europe</span>
							<span className="date-text">13September 2021</span>
					      	<h3>Why you should go to France</h3>
					      	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbiequat laoreet. Vivamus vel nulla at dui suscipit placerat. Cras mattis ipsum ut ex aliquam posuere. uscipit placerat. </p>
							<a href="#" className="ant-btn ant-btn-primary">Read More</a>
					    </div>
				    </Col>

				    <Col md={14}>
					    <Row gutter={[10, 20]}>
					    	<Col xs={24} md={12} className="destination-item">
						      	<img src={DestinationsImage2} />
						      	<div className="destination-item-overlay">
							      	<p className="destination-item-desc">
							      		<span>Why you should go to THAILAND</span>
							      		<a href="#">ASIA</a>
							      	</p>
							    </div>
						    </Col>

						    <Col xs={24} md={12} className="destination-item">
						      	<img src={DestinationsImage3} />
						      	<div className="destination-item-overlay">
						      		<p className="destination-item-desc">
							      		<span>Why you should go to INDONESIA</span>
							      		<a href="#">ASIA</a>
						      		</p>
						      	</div>
						    </Col>

						    <Col xs={24} md={12} className="destination-item">
						      	<img src={DestinationsImage4} />
						      	<div className="destination-item-overlay">
							      	<p className="destination-item-desc">
							      		<span>Why you should go to CANADA</span>
							      		<a href="#">AMERICA</a>
							      	</p>
							    </div>
						    </Col>

						    <Col xs={24} md={12} className="destination-item">
						      	<img src={DestinationsImage5} />
						      	<div className="destination-item-overlay">
							      	<p className="destination-item-desc">
							      		<span>Why you should go to BRAZIL</span>
							      		<a href="#">AMERICA</a>
							      	</p>
							    </div>
						    </Col>
					    </Row>
				    </Col>			    
				</Row>

				<div className="text-center" style={{ margin: '40px 0px' }}>
					<a href="#" className="normal-link">More Destinations <ArrowRightOutlined /></a>
				</div>
			</div>

			<div className="full-width-white">	
				<div className="container">	
					<div className="desti-bottom-section">
						<h1>EXPERIENCE  THE BEST OF <span className="primary-text">EVO HOLIDAY</span></h1>
						<p>
							Lorem ipsum dolor sit amet,magnahabemusius ad,quiminimum voluptaria in.Ad mei modus quodsi complectitur, postea complectiturposteaLorem ipsum dolor sit amet,magn
 						</p>
 						<a href="#" className="primary-link">Read More <ArrowRightOutlined /></a>
					</div>
				</div>
			</div>
		</>
	);
}

export default Destinations;