import React from 'react';
import { Row, Col } from 'antd';

import './Tours.css';

const Tours = () => {
	return (
		<div className="container">
			<Row gutter={[30, 30]}>
      			<Col xs={24} md={8} className="tour-item">
			      	<h2>01 <span>Travel</span></h2>
			      	<p>Sponsorships are like unicorns or leprechauns, talked about often but they don’t actually exist. There is only dollars and cents, the</p>
			    </Col>

			    <Col xs={24} md={8} className="tour-item">
			      	<h2>02 <span>Experience</span></h2>
			      	<p>My response is usually harsh. Offended at the suggestion that a career that’s taken more than a decade to create could be</p>
			    </Col>

			    <Col xs={24} md={8} className="tour-item">
			      	<h2>03 <span>Relax</span></h2>
			      	<p>My response is usually harsh. Offended at the suggestion that a career that’s taken more than a decade to create could be</p>
			    </Col>
    		</Row>
		</div>
	);
}

export default Tours;