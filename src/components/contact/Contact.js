import React from 'react';
import { Form, Input, Button } from 'antd';

import './Contact.css';

const Contact = () => {
	return (
		<div className="full-width-white">
			<div className="container">
				<h1>LEAVE YOUR FEEDBACK</h1>
				<Form className="contact-form">
					<Form.Item
				        name="name"
				        rules={[{ required: true, message: 'Please input Name!' }]}
				        style={{ display: 'inline-block', width: 'calc(50% - 20px)', margin: '10px' }}
				    >
				        <Input placeholder="Name" />
			    	</Form.Item>

			    	<Form.Item
				        name="email"
				        rules={[{ required: true, message: 'Please input E-mail!' }]}
				        style={{ display: 'inline-block', width: 'calc(50% - 20px)', margin: '10px' }}
				    >
				        <Input placeholder="E-mail" />
			    	</Form.Item>

			    	<Form.Item
				        name="subject"
				        rules={[{ required: true, message: 'Please input Subject!' }]}
				        style={{ display: 'inline-block', width: 'calc(100% - 20px)', margin: '10px' }}
				    >
				        <Input placeholder="Subject" />
			    	</Form.Item>

			    	<Form.Item
				        name="message"
				        rules={[{ required: true, message: 'Please input Message!' }]}
				        style={{ display: 'inline-block', width: 'calc(100% - 20px)', margin: '10px' }}
				    >
				        <Input type="textarea" placeholder="Message" style={{ height: '124px' }} />
			    	</Form.Item>

			    	<Button htmlType="submit" className="blue-btn" style={{ width: '303px', height: '53px', padding: '10px 25px', 'border-radius': '10px', margin: '20px 10px' }}>
			          Submit Now
			        </Button>

				</Form>
			</div>
		</div>
	);
}

export default Contact;