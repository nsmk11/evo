import React from 'react';
import { Row, Col, Radio, Input, Button, DatePicker } from 'antd';
import {
  SettingFilled,
  TeamOutlined,
  CalendarFilled,
  SendOutlined
} from '@ant-design/icons';

const SearchFlights = () => {
	const [value, setValue] = React.useState('return');

	const onChange = (e) => {
		setValue(e.target.value);
	}

	return (
		<div className="container search-section">
			<Row gutter={[20, 20]}>
		      <Col xs={24} md={24}>
		      	<Radio.Group onChange={onChange} value={value}>
				      <Radio value='return'>Return</Radio>
				      <Radio value='oneway'>One-Way</Radio>
				    </Radio.Group>
		      </Col>
		    
		      <Col xs={12} md={8} className="input-divider">
		      	<Input placeholder="From" prefix={<SendOutlined />} />
		      </Col>

		      <Col xs={12} md={8} className="input-divider">
		      	<Input placeholder="To" prefix={<SendOutlined />} />
		      </Col>

		      <Col xs={12} md={4} className="input-divider">
		      	<DatePicker placeholder="Depart" prefix={<CalendarFilled />} />
		      </Col>

		      <Col xs={12} md={4} className="input-divider">
		      	<DatePicker placeholder="Return" prefix={<CalendarFilled />} />
		      </Col>

		      <Col xs={12} md={8} className="input-divider">
		      	<Input placeholder="Class" prefix={<SettingFilled />} />
		      </Col>

		      <Col xs={12} md={8} className="input-divider">
		      	<Input placeholder="Traveller" prefix={<TeamOutlined />} />
		      </Col>

		      <Col xs={24} md={8} className="input-divider">
		      	<Button type="primary" className="full-btn">Search Flights</Button>
		      </Col>

		    </Row>
		</div>
	);
}

export default SearchFlights;