import React from 'react';
import { Row, Col, Button } from 'antd';
import { LeftOutlined, RightOutlined } from '@ant-design/icons'; 
import SearchFlights from './SearchFlights';
import { Carousel } from 'react-responsive-carousel';

import "react-responsive-carousel/lib/styles/carousel.min.css";
import './Home.css';
import BackgroundImage from '../../assets/images/Background.png';

const Home = () => {
	return (
		<>
		<div className="slider">
			<Carousel showStatus={false} showThumbs={false} showArrows={true} showIndicators={false}>
				<div>
			     <img src={BackgroundImage} />
            <div className="legend">
              <h2>Nature Awaits</h2>
              <p>
              	<span>Explore The World With Us</span>
              </p>	              
              <a href="#" className="link-btn">LEARN MORE</a>
            </div>
        </div>
	      <div>
           <img src={BackgroundImage} />
            <div className="legend">
              <h2>Nature Awaits</h2>
              <p>
                <span>Explore The World With Us</span>
              </p>                
              <a href="#" className="link-btn">LEARN MORE</a>
            </div>
        </div>
        </Carousel>

        <SearchFlights />

      </div>
      </>
	);
}

export default Home;