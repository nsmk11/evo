import React from 'react';

var ScrollAnim = require('rc-scroll-anim');
var Link = ScrollAnim.Link;

const Header = () => {
	return (
	  <div className="header" id="top">
        <div className="container">
            <div className="logo">
              <a href="/" className="logo-text">EVO</a>
            </div>
            <div className="nav">
              <Link className="nav-list" to="home">Home</Link>
              <Link className="nav-list" to="tours">Tours</Link>
              <Link className="nav-list" to="destinations">Destinations</Link>
              <Link className="nav-list" to="packages">Packages</Link>
              <Link className="nav-list" to="gallery">Gallery</Link>
              <Link className="nav-list" to="contactus">Contact Us</Link>
            </div>            
          </div>
      </div>     
	);
}

export default Header;