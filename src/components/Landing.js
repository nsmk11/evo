import React from 'react';
import Home from './home/Home';
import Tours from './tours/Tours';
import Destinations from './destinations/Destinations';
import Packages from './packages/Packages';
import Gallery from './gallery/Gallery';
import Contact from './contact/Contact';

var ScrollAnim = require('rc-scroll-anim');
var Element = ScrollAnim.Element;

const Landing = () => {
	return (
		<>
		  <Element className="pack-page" id="home"><Home /></Element>
	      <Element className="pack-page" id="tours"><Tours /></Element>
	      <Element className="pack-page" id="destinations"><Destinations /></Element>
	      <Element className="pack-page" id="packages"><Packages /></Element> 
	      <Element className="pack-page" id="gallery"><Gallery /></Element>           
	      <Element className="pack-page" id="contactus"><Contact /></Element> 
		</>
	);
}

export default Landing;