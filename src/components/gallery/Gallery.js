import React from 'react';
import {
  UserOutlined,
  MessageOutlined
} from '@ant-design/icons';
import Carousel from 'react-multi-carousel';
import './Gallery.css';
import 'react-multi-carousel/lib/styles.css';

import Avatar from '../../assets/images/avatar.png';

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1
  }
};

const Gallery = () => {
	return (
		<>
		<div className="gallery-section">
			<div className="container">
				<h3>Our Gallery</h3>
				<div className="gallery-info">
					<h1>See Iceland Through Our Eyes As We Capture Nature, Culture & Everyday Life</h1>
					<p>Take a look at our gallery that features the best moments from our tours and excursions.</p>
					<a href="#" className="ant-btn blue-btn">VIEW GALLERY</a>
				</div>
			</div>
		</div>

		<div className="container testimonial-section">
			<h1>WHAT PEOPLE SAY <MessageOutlined /></h1>
			<Carousel responsive={responsive}>
				<div className="testimonial-box">
					<img src={Avatar} />
					<p>Take a look at our gallery that features the best moments from our tours and excursions.</p>
					<p className="name-text"><UserOutlined />Rupert Wood</p>
				</div>

				<div className="testimonial-box testimonial-box-active">
					<img src={Avatar} />
					<p>We had a marvelous time in our travels to Madagascar, Zimbabwe, and Botswana, we had just wonderful experiences. Your service was amazing and everyone was very attentive!</p>
					<p className="name-text"><UserOutlined />Sam Peterson</p>
				</div>

				<div className="testimonial-box">
					<img src={Avatar} />
					<p>Just wanted to say many, many thanks for helping me set up an amazing Costa Rican adventure! My nephew and I had a great time! All of the accommodations were perfect, thank you!</p>
					<p className="name-text"><UserOutlined />Catherine Williams</p>
				</div>
			</Carousel>
		</div>
		</>
	);
}

export default Gallery;