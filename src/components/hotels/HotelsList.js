import React, { useState,useEffect } from 'react';
import { List, Button, Input, Row, Col, Collapse } from 'antd';
import Multiselect from 'multiselect-react-dropdown';
import {
  StarOutlined,
  DoubleRightOutlined
} from '@ant-design/icons';

import './List.css';

const { Panel } = Collapse;

const categoryList = [
	{
		category: '3 STARS'
	},
	{
		category: '4 STARS'
	},
	{
		category: '4 STARS AND A HALF'
	},
	{
		category: '5 STARS'
	},
	{
		category: '5 STARS LUXURY'
	}
];

const HotelsList = () => {
	const [ hotelsData, setHotelsData ] = useState([]);
	const [ filterName, setFilterName ] = useState('');
	const [ filterCategory, setFilterCategory ] = useState([]);

	const getHotelsData = () => {
		fetch("hotels2.json", {
			headers: {
				'Content-Type': 'application/json',
        		'Accept': 'application/json'
			}
		})
		.then((response) => response.json())
		.then((data) => {
			setHotelsData(data.results.hotels)
		})
	}

	const handleNameChange = (e) => {
		setFilterName(e.target.value);
	}

	const onSelectCategory = (selectedList, selectedItem) => {
	   setFilterCategory(selectedList);
	}

	const onRemoveCategory = () => {
	   
	}

	const filterData = () => {
		let result = hotelsData;

		if(filterName) {
			result = hotelsData.filter((hotel) => hotel.name.toLowerCase().includes(filterName.toLowerCase()));			
		}

		if(filterCategory.length > 0) {			
			result = result.filter(( { categoryName }) => {							
				for ( const cat of filterCategory ) {
				    if (categoryName.toLowerCase().includes(cat.category.toLowerCase())) {
				      return true;
				    }
				}
			});
		}
		
		console.log(result);
		setHotelsData(result);

	}

	useEffect(() => {
		getHotelsData();
	}, [])

	return (
		<div className="container">
			<div className="filter-section">
				<Row gutter={[30, 30]}>
					<Col xs={12} md={8}>
				      	<Input value={filterName} onChange={handleNameChange} />
				    </Col>

						<Col xs={12} md={8}>
				      	<Multiselect
									options={categoryList} 
									onSelect={onSelectCategory} 
									onRemove={onRemoveCategory} 
									displayValue="category"
									showCheckbox
								/>
				    </Col>

				    <Col xs={12} md={8}>
				      	<Button onClick={filterData} type="primary">Filter</Button>
				    </Col>
				</Row>				 
			</div>

			<div>
				<List
			      dataSource={hotelsData}
			      renderItem={hotel => (
			        <List.Item>			          
			          <div className="list-box">
			          	<h4>{hotel.name} <a href={`http://www.google.com/maps/place/${hotel.latitude},${hotel.longitude}`} target="_blank">Google Map <DoubleRightOutlined /></a></h4>
			          	<p className="rating-text"><StarOutlined /> {hotel.categoryName}</p>

			          	<Collapse accordion>
						          	{hotel.rooms.length > 0 && hotel.rooms.map(room => (
						          		<Panel header={room.name}>
									      <p>{room.code}</p>
									      <ul>
									      	{room.rates.length > 0 && room.rates.map(rate => (
									      		<li>{rate.boardName} - $ {rate.price}</li>
									      	))}							      	
									      </ul>
									    </Panel>
						          	))}						    
									</Collapse>

			          </div>
			        </List.Item>
			      )}
			    />
			</div>
		</div>
	);
}

export default HotelsList;