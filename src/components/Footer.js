import React from 'react';
import { Row, Col } from 'antd';
import {
  UpOutlined
} from '@ant-design/icons';

import PaymentImage from '../assets/images/pricebreaker-payment-methods.png';

const Footer = () => {
	return (
		<footer>
			<div className="container">
				<Row gutter={[25, 25]}>
					<Col md={6}>
						<a href="/" className="logo-text">EVO</a>
						<p>Lorem ipsum dolor sit amet,magnaha ad,quiminimum voluptaria complectiturposte </p>
					</Col>

					<Col md={5}>
						<span className="foot-title">Address</span>
						<span className="divider">Tel : (65) 0965243</span>
						<span className="divider">No.34,Sheun Road ,23</span>
						<span>sjholiday@gmail.com</span>
					</Col>

					<Col md={5}>
						<span className="foot-title">Legal</span>
						<a href="#">Terms & Condition</a>
						<a href="#">Privacy Statement</a>
					</Col>

					<Col md={6}>
						<img src={PaymentImage} />
					</Col>
				</Row>

				<a href="#top" class="back-to-top-link" aria-label="Scroll to Top"><UpOutlined /></a>
			</div>
		</footer>
	);
}

export default Footer;