import React from 'react';
import Carousel from 'react-multi-carousel';
import {
  ClockCircleFilled,
  RightOutlined
} from '@ant-design/icons';
import 'react-multi-carousel/lib/styles.css';
import './Packages.css';

import PackagesImage1 from '../../assets/images/joshua-rawson-harris-S4ZkQykkeRs-unsplash 1.png';
import PackagesImage2 from '../../assets/images/artem-beliaikin-rFbLkB33lZQ-unsplash 1.png';
import PackagesImage3 from '../../assets/images/nicolas-i-tVTvDFkT0Vw-unsplash 2.png';

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1
  }
};

const Packages = () => {
	return (
		<div className="container">
			<h1>Popular Tour Packages</h1>
			<p>
				Lorem ipsum dolor sit amet,magnahabemusius ad,quiminimum voluptaria in.Ad mei modus quodsi complectitur, postea complectiturposteaLorem ipsum dolor sit amet.
 			</p>

			<Carousel responsive={responsive}>
			  <div className="carousel-multi-box">
			  	<div className="carousel-img">
					  	<span className="cntr-label">THAILAND</span>
					  	<span className="price-label">$ 400 / Person</span>
					  	<img src={PackagesImage1} />
					  	
					</div>
					<div className="carousel-desc">
					  	<span className="date-label"><ClockCircleFilled /> 5 Days and 6 Nights</span>
					  	<h6>Bangkock is the capital and most populous city of Thailand</h6>
					  	<p>Lorem ipsum dolor sit amet,magnaha ad,quiminimum voluptaria complectiturposte </p>
					  	<a href="#" className="primary-link">More Details <RightOutlined /></a>
					</div>
			  </div>

			  <div className="carousel-multi-box">
				  	<div className="carousel-img">
					  	<span className="cntr-label">INDONESIA</span>
					  	<img src={PackagesImage2} />
					  	<span className="price-label">$ 300 / Person</span>
					</div>
					<div className="carousel-desc">
					  	<span className="date-label"><ClockCircleFilled /> 4 Days and 3 Nights</span>
					  	<h6>Bali island is thewesternmost of the Lesser Sunda Islands</h6>
					  	<p>Lorem ipsum dolor sit amet,magnaha ad,quiminimum voluptaria complectiturposte </p>
					  	<a href="#" className="primary-link">More Details <RightOutlined /></a>
					</div>
			  </div>

			  <div className="carousel-multi-box">
				  	<div className="carousel-img">
					  	<span className="cntr-label">FRANCE</span>
					  	<img src={PackagesImage3} />
					  	<span className="price-label">$ 600 / Person</span>
					</div>
					<div className="carousel-desc">
					  	<span className="date-label"><ClockCircleFilled /> 4 Days and 3 Nights</span>
					  	<h6>Paris  is the capital and most populous city of France</h6>
					  	<p>Lorem ipsum dolor sit amet,magnaha ad,quiminimum voluptaria complectiturposte </p>
					  	<a href="#" className="primary-link">More Details <RightOutlined /></a>
					</div>
			  </div>

			  <div className="carousel-multi-box">
				  	<div className="carousel-img">
					  	<span className="cntr-label">FRANCE</span>
					  	<img src={PackagesImage3} />
					  	<span className="price-label">$ 600 / Person</span>
					</div>
					<div className="carousel-desc">
					  	<span className="date-label"><ClockCircleFilled /> 4 Days and 3 Nights</span>
					  	<h6>Paris  is the capital and most populous city of France</h6>
					  	<p>Lorem ipsum dolor sit amet,magnaha ad,quiminimum voluptaria complectiturposte </p>
					  	<a href="#" className="primary-link">More Details <RightOutlined /></a>
					</div>
			  </div>

			</Carousel>

			<div className="text-center" style={{ margin: '40px 0px' }}><a href="#" className="ant-btn ant-btn-primary">More Packages</a></div>
		</div>
	);
}

export default Packages;