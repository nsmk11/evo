import { Row, Col } from 'antd';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Header from './components/Header';
import Footer from './components/Footer';
import Landing from './components/Landing';
import HotelsList from './components/hotels/HotelsList';

import './App.css';
import "antd/dist/antd.css";

function App() {
  return (
    <div className="App">
      <Header />
      
      <Router>
        <Switch>
          <Route path="/" exact><Landing /></Route>
          <Route path="/hotels"><HotelsList /></Route>
        </Switch>
      </Router>

      <Footer />         
    </div>
  );
}

export default App;
